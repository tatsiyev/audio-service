package grpc

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/tatsiyev/audio-service/audio"
	pb "gitlab.com/tatsiyev/audio-service/protocol/grpc/audiostream"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/types/known/emptypb"
)

type AudioService struct {
	pb.UnimplementedAudioStreamServer
}

func (s *AudioService) GetFormat(context.Context, *emptypb.Empty) (*pb.AudioFormat, error) {
	return nil, nil
}
func (s *AudioService) GetStream(args *emptypb.Empty, stream pb.AudioStream_GetStreamServer) error {
	chunks, err := audio.SplitWavFileIntoChunks("content/korol_i_shut-durak_i_molniya.wav", 0.2)
	if err != nil {
		return nil
	}
	i := 0
	// fmt.Println("start streaming")
	// exit := make(chan interface{})

	// go func() {
	// 	var x *int
	// 	e := stream.RecvMsg(x)
	// 	if e != nil {
	// 		fmt.Println("recive ", e)
	// 		if e == io.EOF {
	// 			exit <- nil
	// 		}
	// 	}
	// }()

	// file := audio.NewAudioReader("content/korol_i_shut-durak_i_molniya.wav")
	var position time.Duration = 0

	// bytes := make([]byte, 8820)

	ticker := time.NewTicker(200 * time.Millisecond)
	for {
		select {
		case <-ticker.C:
			// part, err := file.NextPart()
			// if err != nil {
			// 	return fmt.Errorf("end file")
			// }

			// for i, v := range part.Data {
			// 	bytes[i] = byte(v)
			// }
			if i == len(chunks) {
				return nil
			}
			bytes := chunks[i]

			i++

			stream.Send(&pb.AudioSample{
				Position: float64(position) / float64(time.Second),
				Data:     bytes,
			})
			position += 200 * time.Millisecond

			fmt.Println("send next part")

		case <-stream.Context().Done():
			fmt.Println("disconected")
			return nil

			// case <-exit:
			// 	fmt.Println("disconected")
			// 	return nil
		}
	}
}

func NewAudioService(grpcServer grpc.ServiceRegistrar) {
	pb.RegisterAudioStreamServer(grpcServer, &AudioService{})
}
