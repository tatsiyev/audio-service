package main

import (
	"fmt"
	"net"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	grpc_api "gitlab.com/tatsiyev/audio-service/protocol/grpc"
)

func initGrpc() (net.Listener, *grpc.Server, error) {
	listener, err := net.Listen("tcp", fmt.Sprintf(":%s", "5555"))
	if err != nil {
		return nil, nil, fmt.Errorf("failed to listen: %v", err)
	}
	opts := []grpc.ServerOption{}
	grpcServer := grpc.NewServer(opts...)
	reflection.Register(grpcServer)
	return listener, grpcServer, nil
}

func main() {

	eChan := make(chan error)

	listener, server, err := initGrpc()
	if err != nil {
		fmt.Println(err)
		return
	}
	serveGRPC := func() {
		grpc_api.NewAudioService(server)
		eChan <- server.Serve(listener)
	}

	go serveGRPC()

	fmt.Println("started")

	select {
	case <-eChan:
		return
	}

}
