gen:
	protoc -I=./proto --go_out=./protocol/grpc --go-grpc_out=./protocol/grpc ./proto/*.proto; 

convert-to-ogg:
	ffmpeg -i input.file -c:a libopus -page_duration 20000 -vn output.ogg

convert-to-wav:
	ffmpeg -i input.file -acodec pcm_u8 -ar 22050 output.ogg

push:
	GIT_SSH_COMMAND="ssh -i ~/.ssh/id_rsa_gitlab" git push
