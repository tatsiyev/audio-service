package audio

import (
	"fmt"
	"log"
	"os"

	"github.com/go-audio/audio"
	"github.com/go-audio/wav"
)

type AudioReader struct {
	music      *wav.Decoder
	dataBuffer *audio.IntBuffer
}

func (r *AudioReader) NextPart() (*audio.IntBuffer, error) {
	_, err := r.music.PCMBuffer(r.dataBuffer)
	if err != nil {
		return nil, err
	}
	return r.dataBuffer, nil
}

// "content/korol_i_shut-durak_i_molniya.wav"
func NewAudioReader(filename string) *AudioReader {
	f, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	// defer f.Close()

	music := wav.NewDecoder(f)

	music.ReadInfo()
	fmt.Printf("%#v/n", music)

	// dur, _ := music.Duration()
	// fmt.Println(music.AvgBytesPerSec, dur)

	// music.FwdToPCM()

	// chunk := music.PCMChunk
	return &AudioReader{
		music:      music,
		dataBuffer: &audio.IntBuffer{Format: audio.FormatStereo44100, Data: make([]int, 8820), SourceBitDepth: 8},
	}
}

func SplitWavFileIntoChunks(inputPath string, chunkDuration float64) ([][]byte, error) {
	file, err := os.Open(inputPath)
	if err != nil {
		return nil, fmt.Errorf("error opening file: %w", err)
	}
	defer file.Close()

	decoder := wav.NewDecoder(file)
	fullBuf, err := decoder.FullPCMBuffer()
	if err != nil {
		return nil, fmt.Errorf("error decoding WAV file: %w", err)
	}

	sampleRate := decoder.SampleRate
	numChannels := decoder.NumChans
	framesPerChunk := int(float64(sampleRate) * chunkDuration)

	var chunks [][]byte
	for i := 0; i < len(fullBuf.Data); i += framesPerChunk {
		end := i + framesPerChunk
		if end > len(fullBuf.Data) {
			end = len(fullBuf.Data)
		}
		chunk := convertIntToByte(fullBuf.Data[i:end], int(numChannels))
		chunks = append(chunks, chunk)
	}

	return chunks, nil
}

func convertIntToByte(data []int, numChannels int) []byte {
	byteData := make([]byte, len(data)*numChannels*2) // 2 bytes per sample
	for i, sample := range data {
		for c := 0; c < numChannels; c++ {
			byteData[i*numChannels*2+c*2] = byte(sample & 0xFF)
			byteData[i*numChannels*2+c*2+1] = byte((sample >> 8) & 0xFF)
		}
	}
	return byteData
}

// func File() {
// 	f, err := os.Open("content/korol_i_shut-durak_i_molniya.wav")
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	defer f.Close()

// 	music := wav.NewDecoder(f)

// 	music.ReadInfo()

// 	dur, _ := music.Duration()
// 	fmt.Println(music.AvgBytesPerSec, dur)

// 	music.FwdToPCM()

// 	chunk := music.PCMChunk
// 	fmt.Println(string(chunk.ID[:]), chunk.Pos, chunk.Size)

// 	buf := &audio.IntBuffer{Format: audio.FormatMono44100, Data: make([]int, 8820), SourceBitDepth: 8}

// 	_, err = music.PCMBuffer(buf)
// 	_, err = music.PCMBuffer(buf)
// 	_, err = music.PCMBuffer(buf)
// 	_, err = music.PCMBuffer(buf)
// 	_, err = music.PCMBuffer(buf)
// 	fmt.Println(err)
// 	fmt.Printf("%#v\n", buf)

// 	// fmt.Println(riff.DataFormatID)
// 	// fmt.Println(riff.FmtID)
// 	// fmt.Println(riff.RiffID)
// 	// fmt.Println(riff.WavFormatID)

// 	// var chunk *riff.Chunk
// 	// for {
// 	// 	chunk, err = music.NextChunk()
// 	// 	if err != nil {
// 	// 		fmt.Println("err:", err)
// 	// 		break
// 	// 	}
// 	// 	fmt.Println("new chunk")

// 	// 	// r := make([]byte, chunk.Size)
// 	// 	// chunk.R.Read(r)

// 	// 	fmt.Println(string(chunk.ID[:]), chunk.Pos, chunk.Size)

// 	// 	chunk.Done()
// 	// }

// 	// fmt.Printf("%#v\n", music)

// }
